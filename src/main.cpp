// #define DEBUG 1
// #define SERIAL_BPS 115200
#define VERSION "010"

/**
 *  settings:
 *  Generic ESP8266
 *  Flash mode QIO
 *  Flash size 1MB (no SPIFFS)
 */

/**
 * pin layout of ESP-01 seen antenna top, components facing viewer
 *  GND  - GPIO2 - GPIO0 -  RX
 *  TX   - CP_PD -  RST  - VCC
 */

/**
 * New new wiring, use RT9013 as refulator
 * Battery plus to Vbat (input of RT9013)
 * RST to Vcc to enable
 * CH_PD to Vcc to enable
 * GPIO2 to Vcc for powerup
 * GPIO0 to ENA of RT9013 through a signal diode and a 100K resistor
 * ENA of RT9013 to a 100K resistor and 100nF capacitor in parallel to ground
 * ENA of RT9013 to flap switch to Vbat
 * 
 * Do NOT use an ESP-01S module, as the pullup resistor on GPIO0 will
 * not reliably disable the RT9013 * 
 */

#include "config-jm.h"
#include <ESP8266WiFi.h>
#include "PubSubClient.h"
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#define RTC_OK 0
#define RTC_NOWIFI 1
#define RTC_NOMQTT 2
#define RTC_UPDATED 3
#define RTC_NOPUBLISH 4
#define RTC_OTAFAIL 5

char msgBuf[128];
uint32_t rtc;

ADC_MODE(ADC_VCC);

#ifdef mqtt_ssl
WiFiClientSecure wiFiClient;
#endif
#ifndef mqtt_ssl
WiFiClient wiFiClient;
#endif
PubSubClient pubSubClient(wiFiClient);

template <typename Generic>
void debugMsgLn(Generic g)
{
#ifdef DEBUG
	Serial.println(g);
#endif
}

template <typename Generic>
void debugMsg(Generic g)
{
#ifdef DEBUG
	Serial.print(g);
#endif
}

/**
 * callback function for pubsub. We do nothing as we don't expect any message
 * for the mailbox
 */
void callback(char *topic, byte *payload, unsigned int length)
{
}

/**
 * return quality as a reasonable percentage, more human readable
 */
int WifiGetRssiAsQuality(int rssi)
{
	int quality = 0;

	if (rssi <= -100)
	{
		quality = 0;
	}
	else if (rssi >= -50)
	{
		quality = 100;
	}
	else
	{
		quality = 2 * (rssi + 100);
	}
	return quality;
}

/**
 * callback function for lightsleep. Do essentially nothing. This weird code
 * is required to let light sleep wake up properly
 * https://kevinstadler.github.io/notes/esp8266-deep-sleep-light-sleep-arduino/
 */
void light_sleep_callback()
{
	Serial.end();
	Serial.begin(9600);
	Serial.println("Light sleep is over, either because timeout or external interrupt");
	Serial.flush();
}

/**
 * Stop th eprocessor, but keep GPIOs enabled. The necessary steps to enter timed
 * sleep are a bit fickle, see https://github.com/esp8266/Arduino/issues/7055 for more details
 * but for the simple get-it-done: 
 * https://www.mischianti.org/2019/11/21/wemos-d1-mini-esp8266-the-three-type-of-sleep-mode-to-manage-energy-savings-part-4/
 */
void light_sleep () {
		

		uint32_t sleep_time_in_ms = 10000;
		wifi_set_opmode(NULL_MODE);
		wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);
		wifi_fpm_open();
		wifi_fpm_set_wakeup_cb(light_sleep_callback);
		wifi_fpm_do_sleep(sleep_time_in_ms * 1000);
		delay(sleep_time_in_ms + 1);
}

void powerDown(bool immediate)
{
	// save last status
	ESP.rtcUserMemoryWrite(0, &rtc, sizeof(rtc));
	if (!immediate)
	{
		delay(1);
		WiFi.mode(WIFI_OFF);
		delay(10);
		debugMsgLn(F("Zzz..."));

		// Multiple triggers when the postman is fiddling with the flap are avoided by keeping the
		// device going in a fairely low power state for 10 more seconds
		light_sleep();
	}
	// power down
	digitalWrite(0, LOW); // ESP will power down
	pinMode(0, INPUT); // cap on the LDO has aa bleed
	delay(100); // chip should powerdown somewhere here

	// if not, ie the switch is still closed, goto deepsleep
	// however, the watchdog will probably have kicked in
	ESP.deepSleep(0, WAKE_RF_DEFAULT); // one minute, but since GPIO16 is not connected to RST, nothing happens.
	delay(5);
}

void setup()
{
	char miniBuf[10];
	uint16_t vcc;

	// hold power on
	pinMode(0, OUTPUT);
	digitalWrite(0, HIGH);

	// immediately get Vcc
	vcc = ESP.getVcc();
	if (vcc < 3000)
	{					  // protect the battery
		powerDown(true); // power down immediately
	}

#ifdef DEBUG
	Serial.begin(SERIAL_BPS);
#endif
	debugMsgLn(F(""));
	debugMsg(F("PoBox "));
	debugMsgLn(VERSION);

	// Now start the WiFi https://www.bakke.online/index.php/2017/05/22/reducing-wifi-power-consumption-on-esp8266-part-3/
	debugMsgLn(F("Initialising Wifi..."));
	WiFi.mode(WIFI_OFF);
	WiFi.forceSleepWake();
	delay(1);
	WiFi.persistent(false);
	WiFi.mode(WIFI_STA);
	WiFi.config(ip, gateway, subnet, dns);

	// connect to the WiFi
	if (WiFi.status() != WL_CONNECTED)
	{ // should always be true
		debugMsgLn(F("WiFi.begin"));
		WiFi.begin(homeSsid, homePassword);
	}
	int c = 100; // give it 10 seconds
	while (c-- && WiFi.status() != WL_CONNECTED)
		delay(100);

	// gracefully handle failure, record, and fall asleep
	if (WiFi.status() != WL_CONNECTED)
	{
		rtc = RTC_NOWIFI; // No wifi connection
		debugMsgLn(F("WiFi connection Failed!"));
		powerDown(true); // power down
	}
	debugMsg(F("Connected to: "));
	debugMsg(WiFi.SSID());
	debugMsg(F(", IP address: "));
	debugMsgLn(WiFi.localIP());

	// connect to the MQTT broker
	pubSubClient.setServer(mqtt_server, mqtt_port);
	pubSubClient.setCallback(callback);
	if (!pubSubClient.connect(mqtt_clientId, mqtt_username, mqtt_password))
	{
		rtc = RTC_NOMQTT; // Connect to WiFi, but not to broker
		debugMsgLn(F("MQTT connection Failed!"));
		powerDown(false); // power down
	}
	debugMsgLn(F("MQTT connected"));

	// repeat power protection under load
	vcc = ESP.getVcc();
	if (vcc < 3000)
	{					 // protect the battery
		powerDown(true); // power down immediately
	}
	// Report Vcc
	strcpy(msgBuf, "{\"vcc\":");
	itoa(vcc, miniBuf, 10);
	strcat(msgBuf, miniBuf);

	// report flap status
	strcat(msgBuf, ",\"flap\":true");

	// report last status in case one or more were missed
	strcat(msgBuf, ",\"prev\":");
	ESP.rtcUserMemoryRead(0, &rtc, sizeof(rtc));
	switch (rtc)
	{
	case 0:
		strcat(msgBuf, "\"ok\"");
		break;
	case 1:
		strcat(msgBuf, "\"nowifi\"");
		break;
	case 2:
		strcat(msgBuf, "\"nomqtt\"");
		break;
	case 3:
		strcat(msgBuf, "\"updated\"");
		break;
	case 4:
		strcat(msgBuf, "\"nopublish\"");
		break;
	case 5:
		strcat(msgBuf, "\"otafail\"");
		break;
	default:
		strcat(msgBuf, "\"other\"");
		break;
	}

	// Report RSSI to MQTT.
	strcat(msgBuf, ",\"RSSI\":\"");
	itoa((int)WifiGetRssiAsQuality(WiFi.RSSI()), miniBuf, 10);
	strcat(msgBuf, miniBuf);
	strcat(msgBuf, "\"");

	// Report version to MQTT.
	strcat(msgBuf, ",\"version\":\"");
	strcat(msgBuf, VERSION);
	strcat(msgBuf, "\"");

	// End JSON string.
	strcat(msgBuf, "}");

	// publish the JSON string
	rtc = RTC_OK; // assume all OK
	if (!pubSubClient.publish(mqtt_topic, msgBuf))
	{
		rtc = RTC_NOPUBLISH; // missed a publish
	}

	// disconnect from MQTT
	delay(10);
	pubSubClient.disconnect();
}

void loop()
{

#ifdef HTTP_OTA_ADDRESS
	// try http ota
	if (WiFi.status() == WL_CONNECTED)
	{
		debugMsgLn(F("Check OTA..."));
		// now try OTA
		switch (ESPhttpUpdate.update(wiFiClient, HTTP_OTA_ADDRESS, HTTP_OTA_PORT, HTTP_OTA_PATH, String(__FILE__).substring(String(__FILE__).lastIndexOf('/') + 1) + "." + VERSION))
		{
		case HTTP_UPDATE_FAILED:
			debugMsgLn("HTTP update failed: Error " + ESPhttpUpdate.getLastErrorString() + "\n");
			rtc = RTC_OTAFAIL;
			break;
		case HTTP_UPDATE_NO_UPDATES:
			debugMsgLn(F("No updates"));
			break;
		case HTTP_UPDATE_OK:
			debugMsgLn(F("Update OK"));
			delay(10000);
			rtc = RTC_UPDATED;
			break;
		}
	}
#endif

	powerDown(false);
}
